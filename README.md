# Testing markdown

Testing markdown for assignment 👍

```mermaid
graph TD
    A(DRINK COFFEE) --> B(YES)
    A --> C(NO)
    B--> D(GOOD CHOICE)
    C --> E(WRONG)
    E --> A
```
